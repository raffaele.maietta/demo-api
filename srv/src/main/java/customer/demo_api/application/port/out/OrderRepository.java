package customer.demo_api.application.port.out;

import customer.demo_api.infrastracture.adapter.persistence.resource.OrderEntity;

public interface OrderRepository {
    void saveOrder(OrderEntity entity);
}
