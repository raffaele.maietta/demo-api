package customer.demo_api.application.port.in;


import customer.demo_api.application.domain.model.Order;

public interface OrderingCoffee {
    Order placeOrder(Order order);
}
