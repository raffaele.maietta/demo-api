package customer.demo_api.application.domain.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Order {
    private String id;
    private String location;
    private List<String> items;
}
