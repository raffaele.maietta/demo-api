package customer.demo_api.application.service;

import customer.demo_api.application.domain.model.Order;
import customer.demo_api.application.port.in.OrderingCoffee;
import customer.demo_api.application.port.out.OrderRepository;
import customer.demo_api.common.UseCase;
import customer.demo_api.infrastracture.adapter.mapper.OrderMapper;
import lombok.extern.slf4j.Slf4j;

@UseCase
@Slf4j
public class OrderingCoffeeUseCase implements OrderingCoffee {

    final OrderRepository orderRepository;
    final OrderMapper orderMapper;

    public OrderingCoffeeUseCase(OrderRepository orderRepository, OrderMapper orderMapper) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
    }

    @Override
    public Order placeOrder(Order order) {
        log.info("Placing order " + order.getId());
        orderRepository.saveOrder(orderMapper.orderToOrderEntity(order));
        return order;
    }
}
