package customer.demo_api.infrastracture.adapter.rest;

import customer.demo_api.application.domain.model.Order;
import customer.demo_api.application.port.in.OrderingCoffee;
import customer.demo_api.infrastracture.adapter.mapper.OrderMapper;
import customer.demo_api.infrastracture.adapter.rest.resource.OrderRequest;
import customer.demo_api.infrastracture.adapter.rest.resource.OrderResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/demo")
public class OrderController {
    private final OrderingCoffee orderingCoffee;
    private final OrderMapper orderMapper;

    OrderController(OrderingCoffee orderingCoffee, OrderMapper orderMapper){
        this.orderingCoffee = orderingCoffee;
        this.orderMapper = orderMapper;
    }
    @PostMapping(value = "/order", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<OrderResponse> createOrder(@RequestBody OrderRequest request, UriComponentsBuilder uriComponentsBuilder) {
        Order order = orderingCoffee.placeOrder(orderMapper.orderRequestToOrder(request));
        URI location = uriComponentsBuilder.path("/order/{id}")
                .buildAndExpand(order.getId())
                .toUri();
        return ResponseEntity.created(location).body(orderMapper.orderToOrderResponse(order));
    }

    /*
    @PostMapping("/order/{id}")
    ResponseEntity<OrderResponse> updateOrder(@PathVariable UUID id, @RequestBody OrderRequest request) {
        var order = orderingCoffee.updateOrder(id, request.toDomain());
        return ResponseEntity.ok(OrderResponse.fromDomain(order));
    }

    @DeleteMapping("/order/{id}")
    ResponseEntity<Void> cancelOrder(@PathVariable UUID id) {
        orderingCoffee.cancelOrder(id);
        return ResponseEntity.noContent().build();
    }
    */
}