package customer.demo_api.infrastracture.adapter.mapper;


import customer.demo_api.application.domain.model.Order;
import customer.demo_api.infrastracture.adapter.persistence.resource.OrderEntity;
import customer.demo_api.infrastracture.adapter.rest.resource.OrderRequest;
import customer.demo_api.infrastracture.adapter.rest.resource.OrderResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.UUID;

@Mapper(componentModel = "spring", imports = UUID.class)
public interface OrderMapper {
    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);
    @Mapping(target = "id", expression = "java(UUID.randomUUID().toString())")
    Order orderRequestToOrder(final OrderRequest request);
    OrderResponse orderToOrderResponse(final Order order);

    OrderEntity orderToOrderEntity(final Order order);
    Order orderEntityToOrder(final OrderEntity entity);
}
