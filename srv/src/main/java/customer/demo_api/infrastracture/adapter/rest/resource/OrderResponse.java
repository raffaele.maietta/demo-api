package customer.demo_api.infrastracture.adapter.rest.resource;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderResponse {
    private String id;
    private String location;
    private List<String> items;
}
