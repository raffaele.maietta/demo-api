package customer.demo_api.infrastracture.adapter.rest.resource;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderRequest {
    String location;
    List<String> items;
}
