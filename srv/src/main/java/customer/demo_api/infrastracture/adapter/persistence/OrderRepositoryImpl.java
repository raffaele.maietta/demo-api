package customer.demo_api.infrastracture.adapter.persistence;

import customer.demo_api.infrastracture.adapter.persistence.resource.OrderEntity;
import customer.demo_api.application.port.out.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderRepositoryImpl implements OrderRepository {
    private final JdbcTemplate jdbcTemplate;

    public OrderRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void saveOrder(OrderEntity entity) {
        log.info("Saving order " + entity.getId());
        //jdbcTemplate.execute(...);
    }
}
