package customer.demo_api.infrastracture.adapter.persistence.resource;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderEntity {
    private String id;
    private String location;
    private List<String> items;
}
