package customer.demo_api.infrastracture.configuration.security;

import com.sap.cloud.security.xsuaa.mock.XsuaaMockWebServer;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Profiles;

public class MockEnvironmentProcessor implements EnvironmentPostProcessor, DisposableBean {
	
	private final XsuaaMockWebServer mockAuthorizationServer = new XsuaaMockWebServer();
	
	@Override
	public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
		if (environment.acceptsProfiles(Profiles.of("default"))) {
            environment.getPropertySources().addFirst(this.mockAuthorizationServer);
        }
	}

	@Override
    public void destroy() throws Exception {
        this.mockAuthorizationServer.destroy();
    }
}